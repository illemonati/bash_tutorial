#!/usr/bin/env bash

read -p "Enter your name: " name

if [[ $name = $USER ]]; then
    echo "Hi, me!"
else
    echo "Hello, $name!"
fi
