greet_person() {
    echo "Hello there, $1"
    python3 -c "print('also hello there ${1}, its me, python')"
}

names=("Jerry" "George" "James" "William" "Paul" "Peter")

for name in ${names[@]}; do
    greet_person $name
done
