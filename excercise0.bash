#!/usr/bin/env bash

# Run the ls program.
ls

# Find out where bash will find the ls program.
type ls

# Show your system's PATH.
echo $PATH

# Create a script in your home directory, add it to your PATH and then run the script as an ordinary command.
script="
    #!/usr/bin/env bash\n
    echo \"Hello Boomer\"
"

file=~/hi_boomer.bash
touch $file
echo -e $script >$file
chmod +x $file
PATH=$PATH:~
hi_boomer.bash
