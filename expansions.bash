#1 /usr/bin/env bash

# Assign hello to the variable greeting.
greeting="hello"

# Show the contents of the variable greeting.
echo "$greeting"

# Assign the string  world to the end of the variable's current contents.
greeting+="world"

array=("hi" "hello" "yes")
echo "${array[@]}"
