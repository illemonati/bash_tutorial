#1 /usr/bin/env bash

# Run a command that produces a message on standard output.
ls

# Run a command that produces a message on standard error.
ls /big/no/existo

# Run a command that produces messages on both standard output and standard error.
ls . /big/no/existo

# Send only the last command's standard error messages to a file called errors.log. Then show the contents of errors.log on the terminal.
ls . /big/no/existo 2>error.log

# Append the last command's standard output and error messages to the file called errors.log. Then show the contents of errors.log on the terminal again.
ls . /big/no/existo &>>error.log

# Use a here-string to show the string Hello world. on the terminal.
cat <<<"Hello World"

# Fix this command so that the message is properly saved into the log file and such that FD 3 is properly closed afterwards: exec 3>&2 2>log; echo 'Hello!'; exec 2>&3
exec 3>&1 1>log
echo 'Hello'
exec 1>&3
3>&-
